# Cumulus #
Cumulative Metrics of Linux Systems
<div style="width:200px; height:140px;float:right"><img src="assets/images/cloud.png" style="width:181px; height:121px; float:right;" alt="Cloud image" /></div>
### OVERVIEW
Cumulus is designed to be an easy way to maintain a list of VMs operated by CoSector, record the state of basic system metrics, and track their change over time.

Cumulus is a simple system for monitoring VMs.  It ingests hostnames, imported via a plain text file, into a MySQL database. It uses a combination of BASH and Python to manage its database, query network endpoints and provide a coherent view of single VMs.

## IN-DEPTH

There are two main processes in this system: the loader, and the runner. The loader attempts to load FQDNs into the database in a rational way, providing a single source of truth for the status of a machine (i.e. whether it's on- or off-line). Once the status of the machine is established, the runner processed it - along with all other online FQDNs, harvesting data from each machine in turn and recording it in the database.

The loader and the runner are designed to run separately, and independently, from each other.

### IMPORTING HOSTS

#### The Loader

The loader is actually comprised of two stages: loader and importer. The loader takes a data source (currently a plain text file, one line per FQDN, is the only supported data source) and loads that data into the vm_loader table, marking the source method in that table too.  The loader filters input text and validates the existence of the FQDN, as far as DNS is aware.

The importer then runs separately against each entry in the vm_loader table. Each FQDN is checked against the database, and then for what type of record it is (CNAME, A and so on). Finally, it checks whether the entity itself exists in reality.

If the DNS record doesn't exist, this item is skipped. If it exists but the server doesn't, then this is flagged in the database. The FQDN could still be picked up for the runner queue, but this will only be done a set number of times before the host is deemed to be permanently offline.  In that situation, it would then be marked as such and no longer picked up by the runner to enumerate host data such as disk sizes.

A future feature allowing manual override in the database is envisaged, which would allow either temporary or permanent checking, despite the machine status at the time.

### CHECKING HOST STATUSES

#### The Runner

The runner is the process that works from the data within the database, and processes one or more queries to each host in turn.

The principle purpose of the runner is to check that the machine exists. Future developments will query each FQDN, taking the form of an SSH call to retrieve a specific data set - be it memory, cpu usage, disk usage and other metrics.

The runner is scheduled to run from cron, every hour. However, it will only run a complete pass of all FQDNs it knows about if the current run queue is empty.  If the queue is not empty (i.e. the runner is still running), the call will politely "fail".  This means it is opportunistic in its data collection, but the frequency of collection is limited to how quickly this single-threaded process can iterate through the entire list of canonical FQDNs.  This is a deliberate design decision to help cement the harvesting process and can be adapted into parallel execution later, based perhaps on customer groups.

### OTHER STUFF

#### Configuration

The system has a database (the schema is included in this repo) which stores VM data. Hostname information is imported via a plain text file, located in the `import` folder. When the import data is fully processed, the import file is deleted.

The database is then the "source" for those VMs.  If the import file contained hostnames of VMs that are already monitored, the system will try to work out what to do with said hostname(s).

#### Requirements and Dependencies
    - BASH
    - Python 3
    -- dnspython
    - MySQL

#### Database configuration

    The schema can be found in `.dbschema.sql`.

#### Tests

    Unit and system tests are not planned at this time.


Created by [Steve Dowe](steve.dowe@cosector.com)
Please send complaints to /dev/null

Version 0.1
