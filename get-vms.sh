#!/bin/bash

# get-vms.sh
#
# The purpose is to be called and produce a single output - a canonical file
# with all our VMs listed in it.

# This script uses three sources of information for a list of hosts:
# 1) The Bloom DNS zone
# 2) The vmlist produced from Azure
# 3) A plain text file, one FQDN per line.

# First check this script isn't already running, by using a lock file

if [ -f get-vms.lck ]; then
    echo $0 is already running. Aborting.
    exit 1
else
    touch get-vms.lck
    # This script is assumed to be running on asutil04.  Check this is the case.
    thisbox=$(hostname | cut -d\. -f1)

    if [ "$thisbox" != "asutil04" ]; then
        echo Please run this script on asutil04.
        exit 1
    else
        # we're good to get VMs from each area.

        # process params
        while getopts h:b opt; do
            case ${opt} in
                h ) # process option h
                    echo "Option -h prints this help".
                    echo "Available commands are:"
                    echo " -h = this help"
                    echo " -b = batch mode. This looks up all DNS entries in the
        Bloom and cdl.cosector.com DNS zones. Use of this option avoids checking
        if hosts are online or offline, and simply outputs a list of those hosts."
                    echo " "
                    echo " "
                    exit 0;
                    ;;
                s ) # the "skip" option
                    skip="$OPTARG"
                    optmsgs="Skipping $skip"
                    ;;
                v ) # process option v
                    echo " "
                    echo "$0 version: $version"
                    echo " "
                    echo "$description"
                    echo " "
                    exit 0;
                    ;;
                *) echo "Usage: $0 [-h] [-s <option>] [-v] hostname"
                   echo " "
                   echo "-s (skip) only supports one of the following options:"
                   echo "sitedata, mysqlsize, all"
                   ;;
            esac
        done


        # azure
        vmsazure=$(ssh jump01 cat vmslist.tsv | grep northeurope | awk '{print $8;}')

        # on-prem (bloom)
        vmsonprem=$(ssh asutil06 cat dns/bloom.ulcc.ac.uk.2021061801 | awk ' { print $1; }' | grep "^[A-Za-z].*\.$" | sed 's/.$//' | grep -v "^bloom.ulcc.ac.uk")

        # really need to sanitise this data before using it

        # one way to do that is check the host exists

        # first, create a megavariable of all fqdns
        allvms="$vmsonprem $vmsazure"

       for h in $(echo "$allvms" | tr " " "\n"); do

            echo -n $h" " ;

           if $(ssh -q -o BatchMode=yes -o ConnectTimeOut=2 -o ConnectionAttempts=1 $h exit);
            then
                echo online;
            else
                echo offline;
            fi
        done

    fi
